## Artificial Intelligence  (AI) in Genomics Diagnostics and Personalized Medicine

This aims at:
1. Preparing catalogue of data related to disease of interest
2. Development/Application of a robust bioinformatics pipeline to study genomic elements utilising
‘multi-omics’ information.
3. Integration of genomic data with health records and other possible information such
as from IoT devices using analytics tools based on machine learning
4. AI and ML models for early diagnosis of diseases and identification of therapeutic
targets
